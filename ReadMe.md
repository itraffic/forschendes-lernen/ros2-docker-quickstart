
# ROS2 Docker Quickstart

Eine ROS2 Installation im Docker Container.

# Verwendung

1. Installiere Docker

Unter Ubuntu 22.04:

### Alte Versionen von Docker entfernen:
```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
```

### Docker Repository hinzufügen:
```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Docker installieren:
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

2. Erlaube non-root Nutzer Zugriff auf Docker

Dies erfolgt normalerweise durch ein Hinzufügen des aktuellen Nutzers zur Gruppe "docker":
```bash
sudo usermod -aG docker $USER
```

Daraufhin muss das **System neu gestartet** werden!

3. Klonen dieser Repository

```bash
git clone git@gitlab.uni-oldenburg.de:itraffic/forschendes-lernen/ros2-docker-quickstart.git
```

4. Ausführen des Haupt-Scripts:

```bash
cd ros2-docker-quickstart
./dev.sh
```

Das Skript erstellt beim ersten Start automatisch einen Docker Container und startet diesen.  
Das Erstellen des Containers kann zwischen 20 Minuten und 2 Stunden dauern, je nach Internet-Verbindung.  
Bei 10 MB/s sollte es 30-40 Minuten dauern.

Um mehr als ein Terminal gleichzeitig zu verwenden, kann das `dev.sh` Skript einfach in einem weiteren Terminal ausgeführt werden.
Beide laufenden Skripte laufen im selben Docker-Container.

Um das Öffnen von Terminals zu beschläunigen, empfehle ich folgenden Alias der `.bashrc` hinzuzufügen:

```bash
alias dev=/path/to/ros2-docker-quickstart/dev.sh
```

Dies ermöglicht es, ein Terminal zu öffnen, `dev` einzutippen und direkt im Docker-Container zu sein.


# Programmierung mit ROS2

In ROS2 gibt es sogenannte Workspaces, Packages und Nodes.

Eine Node ist der Einstiegspunkt (enthält die main-Funktion) eines Packages.  
Ein Package enthält ein oder mehrere Nodes.  
Ein Workspace enthält ein oder mehrere Packages.  
Innerhalb eines Workspaces können Packages voneinander abhängig sein.  

**Wichtig!**  
Bei allen folgenden Befehlen, **muss** man sich im Root-Verzeichnis des Workspace befinden!  
Hier: `/home/dockeruser/ros2_ws`  
Dieser Workspace wird automatisch gesourced.


Normalerweise wird ein ganzer Workspace kompiliert:
```bash
colcon build
```

Man kann auch einzelne Packages kompilieren:
```bash
colcon build --packages-up-to "$PACKAGE_NAME"
```

Um ein neues Package zu erstellen:
```bash
ros2 pkg create --build-type ament_python --node-name "$NODE_NAME" "$PACKAGE_NAME"
```

Um eine Node zu starten, kann man in einem Workspace folgenden Befehl verwenden:
```bash
ros2 run "$PACKAGE_NAME" "$NODE_NAME"
```

Falls es einen Fehler wie `No executable found` gibt, muss der Workspace neu gesourced werden:
```bash
source "$ROS_WORKSPACE_ROOT/install/local_setup.zsh"
```

Dies muss jedes Mal gemacht werden, wenn ein neues Package hinzukommt oder sich etwas an einem Einstiegspunkt eines Packages ändert.


# Code-Editoren / IDEs

Diese Repository enthält bereits einen Ordner namens 'home'.  
Dieser ist als der Nutzerordner innerhalb des Containers gemounted.  
Er befindet sich also auf dem Pfad `/home/dockeruser`.  
Hier kann software extrahiert werden und Code aufbewahrt werden.

Man kann entweder einen Code-Editor innerhalb des Containers oder einen Editor auf dem Host-PC verwenden.  
Der Vorteil eines Editors innerhalb des Containers ist, dass er auf alle Python-Daten, die für Autocomplete benötigt werden, zugreifen kann.

### PyCharm

Ich kann empfehlen, PyCharm in den Home-Ordner zu extrahieren und innerhalb des Containers zu starten.

Damit PyCharm die ROS2 Packete erkennt, müssen zwei Ordner dem Suchpfad für Python-Packages hinzugefügt werden:
```
/opt/ros/humble/lib/python3.10/site-packages
/opt/ros/humble/local/lib/python3.10/dist-packages
```

File > Settings... > Python Interpreter > Python Interpreter: Show all... (Ausklapp-Menü) > Show Interpreter Paths (Icon mit Ordnerstruktur)  
Dort kann man die neuen Pfade in die Liste hinzufügen.

Für PyCharm gibt es außerdem ein Plugin namens `Hatchery`, welches Syntax-Highlighting für ROS2 Configurationsdateien bietet.
