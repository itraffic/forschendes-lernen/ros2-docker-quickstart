# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

if ! test -d "$ZSH"; then
  echo
  echo
  echo
  echo
  echo "================================="
  echo "== Running first time setup... =="
  echo "================================="
  echo
  echo
  echo
  echo
  "$HOME/create_home.sh"
fi

# Theme name
ZSH_THEME="re5et"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="false"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# (now handled above)
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="false"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

HIST_STAMPS="dd.mm.yyyy"

plugins=(
  git
  rust
  zsh-syntax-highlighting
  zsh-autosuggestions
)

# Load oh-my-zsh
source $ZSH/oh-my-zsh.sh

# Use vim for all operations
export EDITOR='vim'
# Use w3m for when a web-browser is needed
export BROWSER='w3m'

# Fix gpg on servers
export GPG_TTY=$(tty)

# Disable Ctrl-S freezing the terminal
stty -ixon

# File Navigation shortcut
alias ..="cd .."

if which exa > /dev/null; then
  alias l="exa -lh --git --group-directories-first --color=always"
  alias ll="exa -lah --git --group-directories-first --color=always"
  alias la="exa -ah --git --group-directories-first --color=always"
  alias tree="exa -Tlah --git --group-directories-first --color=always"
else
  alias l="ls -lh --color=always"
  alias ll="ls -lah --color=always"
  alias la="ls -ah --color=always"
fi

# Better copying
alias cpa="cp -arv"
alias cpr="cp -rv"

# Services
alias restart='sudo systemctl restart'
alias status='sudo systemctl status'
alias start='sudo systemctl start'
alias stop='sudo systemctl stop'

# Some more shortcuts
alias cls="tput reset"
alias chmox="chmod +x"
alias spell="aspell --dont-backup check"
alias :q="exit"

# Git
alias gs="git status"
alias ga="git add"
alias gaa="git add -A"
alias gc="git commit"
alias gp="git push"
alias gpu="git pull"
alias gpull="git pull"
alias gd="git diff"
alias gds="git diff --staged"
alias gunstage="git restore --staged"
alias groot="git rev-parse --show-toplevel"

# Some functions to make life easier

function math() {
  echo $(($@))
}

function tunnel_from {
  if test "$#" -ne 3; then
    echo "Tunnel a connection to a port on a remote machine to a local port" >&2
    echo "Usage: tunnel_from <REMOTE HOST> <LOCAL PORT> <REMOTE PORT>" >&2
  else
    host="$1"
    local_port="$2"
    remote_port="$3"
    ssh -N $host -R ${local_port}:localhost:${remote_port}
  fi
}

function tunnel_to {
  if test "$#" -ne 3; then
    echo "Tunnel a connection to a port on this machine to a remote port" >&2
    echo "Usage: forward <REMOTE HOST> <LOCAL PORT> <REMOTE PORT>" >&2
  else
    host="$1"
    local_port="$2"
    remote_port="$3"
    ssh -N $host -L ${local_port}:localhost:${remote_port}
  fi
}

# Replace the rm command with a function that automatically turns it into "rm -rf", if run on a directory
export RM_PATH="$(which rm)"
function rm() {
  for arg in "$@"; do
    if test -d $arg; then
      "$RM_PATH" -rf $arg
    else
      "$RM_PATH" $arg
    fi
  done
}

# Rust Cargo
if test -f "$HOME/.cargo/env"; then
  source $HOME/.cargo/env
fi

# make flags
export MAKEFLAGS="-j$(nproc)"
#TERM="xterm"

export ROS_DOMAIN_ID=69

export ROS_WORKSPACE_ROOT="/home/dockeruser/ros2_ws"
source /opt/ros/humble/setup.zsh
source "$ROS_WORKSPACE_ROOT/install/local_setup.zsh"

echo 'Always source your workspace!'
echo 'source "$ROS_WORKSPACE_ROOT/install/local_setup.zsh"'
echo 'The folder $HOME/ros2_ws is already sourced and ready.'
echo
echo 'Create a package:'
echo '    ros2 pkg create --build-type "$BUILD_TYPE" --node-name "$NODE_NAME" "$PACKAGE_NAME"'
echo '$BUILD_TYPE = (ament_cmake ament_python ament_cmake_python)'
echo
echo 'Run a node from a package:'
echo '    ros2 run "$PACKAGE_NAME" "$NODE_NAME"'
echo
echo 'Build a package:'
echo '    colcon build --symlink-install'
echo '    colcon build --symlink-install --packages-up-to "$PACKAGE_NAME"'
echo
echo 'Install missing dependencies:'
echo '    rosdep install --from-paths src --rosdistro humble --ignore-src -r -y'

# argcomplete for ros2 & colcon
eval "$(register-python-argcomplete3 ros2)"
eval "$(register-python-argcomplete3 colcon)"

figlet 'ROS2 Docker'

# vim: ts=2:set expandtab:sw=2
