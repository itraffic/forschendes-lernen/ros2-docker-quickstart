
#set -x

file_path="$(realpath $0)"
base_dir="$(dirname $file_path)"

xhost "+SI:localuser:$(id -un)"

recreate_container() {
  docker stop --time 1 ros2-docker-container

  docker rm ros2-docker-container

  docker build --tag ros2-docker-image \
    --no-cache \
    --build-arg DISPLAY="$DISPLAY" \
    --build-arg USER_ID="$(id -u)" \
    --build-arg GROUP_ID="$(id -g)" \
    "${base_dir}/docker/"

  docker create --name ros2-docker-container \
  --attach STDIN --attach STDOUT --attach STDERR \
  --interactive \
  --tty \
  --ipc=host \
  --user "$(id -u):$(id -g)" \
  --volume "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
  --volume "$base_dir/home:/home/dockeruser" \
  --volume "/dev/dri:/dev/dri" \
  --volume "/dev/input:/dev/input" \
  --device-cgroup-rule='c *:* rmw' \
  --hostname ros2docker \
  --network host \
  --add-host ros2docker:127.0.0.1 \
  --add-host localhost:127.0.0.1 \
  ros2-docker-image
}

start_shell_in_container() {
  docker start ros2-docker-container
  docker exec --interactive --tty ros2-docker-container zsh
}


# Recreate the container, if the required parameter is given
if [ "$#" -eq "1" ]; then
  if [ "$1" == "--recreate" ] || [ "$1" == "-r" ]; then
    #echo "Recreation requested"
    recreate_container
  fi
fi

# Check if there is already a running docker container named "ros2docker"
if docker ps | grep ros2-docker-container > /dev/null; then
  #echo "Container running, jumping into a new shell"
  start_shell_in_container
else
  # If there is not yet a container built, build one
  if ! docker container list --all | grep ros2-docker-container > /dev/null; then
    #echo "No container built, creating one..."
    recreate_container
  fi
  #echo "Starting container and opening a shell"
  start_shell_in_container
fi

# vim: ts=2:et:sw=2
